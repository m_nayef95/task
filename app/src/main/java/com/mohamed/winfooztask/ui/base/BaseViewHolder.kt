package com.mohamed.winfooztask.ui.base

import android.support.v7.widget.RecyclerView
import android.view.View

/**
 * Created by Mohamed Hamdan on 2018-Jun-29.
 * mhamdan@nizek.com
 */
abstract class BaseViewHolder(itemView: View?) : RecyclerView.ViewHolder(itemView) {

    abstract fun bind(item: Any?)
}
