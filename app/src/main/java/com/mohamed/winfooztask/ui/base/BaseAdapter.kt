package com.mohamed.winfooztask.ui.base

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.mohamed.winfooztask.R

abstract class BaseAdapter<M, V : BaseViewHolder>(var adapterItems: MutableList<M>?) : RecyclerView.Adapter<V>() {

    protected lateinit var inflater: LayoutInflater
    protected var action: (item: M?, position: Int) -> Unit = { _, _ -> }

    companion object {

        private const val NO_DATA_TYPE = 0
        private const val ITEM_TYPE = 1
    }

    protected abstract fun onBind(holder: V, position: Int)

    protected abstract fun getViewHolder(parent: ViewGroup, viewType: Int): V

    override fun onAttachedToRecyclerView(recyclerView: RecyclerView) {
        super.onAttachedToRecyclerView(recyclerView)
        inflater = LayoutInflater.from(recyclerView.context)
    }

    final override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): V {
        val view: View?
        when (viewType) {
            NO_DATA_TYPE -> {
                view = inflater.inflate(R.layout.row_no_data, parent, false)
                return NoDataHolder(view) as V
            }
        }
        return getViewHolder(parent, viewType)
    }

    final override fun onBindViewHolder(holder: V, position: Int) {
        if (getItemViewType(position) != NO_DATA_TYPE) {
            holder.itemView.setOnClickListener {
                val adapterPosition = holder.adapterPosition
                if (adapterPosition != RecyclerView.NO_POSITION) {
                    action(getItem(adapterPosition), adapterPosition)
                }
            }
            onBind(holder, position)
        }
    }

    override fun getItemViewType(position: Int): Int {
        return if (adapterItems == null || adapterItems?.isEmpty() == true) {
            NO_DATA_TYPE
        } else {
            ITEM_TYPE
        }
    }

    override fun getItemCount(): Int {
        return if (adapterItems?.isEmpty() == true) {
            1
        } else {
            adapterItems?.size ?: 1
        }
    }

    fun setItems(items: MutableList<M>) {
        this.adapterItems = items
    }

    protected fun getItem(position: Int): M? {
        return adapterItems?.getOrNull(position)
    }

    fun setOnItemClickListener(action: (item: M?, position: Int) -> Unit) {
        this.action = action
    }

    private inner class NoDataHolder(itemView: View?) : BaseViewHolder(itemView) {

        override fun bind(item: Any?) {

        }
    }
}
