package com.mohamed.winfooztask.ui.selection.majors

import com.mohamed.winfooztask.models.Major

interface MajorSelectionContract {

    interface Actions {

        fun onSearchTextChanged(text: CharSequence)

        fun onCreate(ids: ArrayList<Int>?)

        fun onDoneClicked()

        fun selectAll(adapterItems: MutableList<Major>?)
    }

    interface View {
        fun setNewData(filteredItems: MutableList<Major>)

        fun setMajors(majors: MutableList<Major>)

        fun goBackWithResult(names: String)

        fun notifyAdapter()
    }
}