package com.mohamed.winfooztask.ui.selection.universities

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import butterknife.ButterKnife
import butterknife.OnTextChanged
import com.mohamed.winfooztask.R
import com.mohamed.winfooztask.models.University
import com.mohamed.winfooztask.ui.base.BaseActivity
import com.mohamed.winfooztask.ui.selection.SelectionAdapter
import kotlinx.android.synthetic.main.activity_university_selection.*

class UniversitySelectionActivity : BaseActivity(), UniversitySelectionContract.View {

    private val actions: UniversitySelectionContract.Actions? by lazy { UniversitySelectionPresenter(this) }
    private var adapter: SelectionAdapter<University>? = null

    companion object {
        const val UNIVERSITY = "university"
        const val UNIVERSITIES = "universities"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_university_selection, true, true)

        ButterKnife.bind(this)
        actions?.onCreate(intent?.getIntegerArrayListExtra(UNIVERSITIES))
    }

    override fun setUniversities(universities: MutableList<University>) {
        adapter = SelectionAdapter(universities)
        rvUniversities?.adapter = adapter
        adapter?.setOnItemClickListener { item, _ ->
            if (item == null) {
                actions?.selectAll(adapter?.adapterItems)
                return@setOnItemClickListener
            }
            item.selected = !item.selected
            adapter?.notifyDataSetChanged()
        }
    }

    override fun setNewData(filteredItems: MutableList<University>) {
        adapter?.updateList(filteredItems)
    }

    override fun goBackWithResult(ids: ArrayList<Int>) {
        val data = Intent()
        data.putExtra(UNIVERSITY, ids)
        setResult(Activity.RESULT_OK, data)
        finish()
    }

    override fun notifyAdapter() {
        adapter?.notifyDataSetChanged()
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_selection, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.i_menu_selection -> actions?.onDoneClicked()
        }
        return super.onOptionsItemSelected(item)
    }

    @OnTextChanged(R.id.etActivityUniversitiesSearch)
    fun onSearchTextChanged(text: CharSequence) {
        actions?.onSearchTextChanged(text)
    }
}