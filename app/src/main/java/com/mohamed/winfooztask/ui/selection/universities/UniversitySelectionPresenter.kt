package com.mohamed.winfooztask.ui.selection.universities

import com.mohamed.winfooztask.common.MyApplication
import com.mohamed.winfooztask.models.University

class UniversitySelectionPresenter(val view: UniversitySelectionContract.View?) : UniversitySelectionContract.Actions {

    private var universities: MutableList<University> = MyApplication.instance.universities

    override fun onCreate(ids: java.util.ArrayList<Int>?) {
        view?.setUniversities(universities)
    }

    override fun onDoneClicked() {
        val ids = universities.filter { it.selected }.map { it.id }
        view?.goBackWithResult(ArrayList(ids))
    }

    override fun onSearchTextChanged(text: CharSequence) {
        view?.setNewData(universities.filter { it.name.contains(text, true) }.toMutableList())
    }

    override fun selectAll(adapterItems: MutableList<University>?) {
        adapterItems?.forEach { it.selected = true }
        view?.notifyAdapter()
    }
}