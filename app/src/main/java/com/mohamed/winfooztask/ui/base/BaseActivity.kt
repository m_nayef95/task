package com.mohamed.winfooztask.ui.base

import android.annotation.SuppressLint
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.Toolbar
import android.view.MenuItem
import com.mohamed.winfooztask.R

/**
 * Created by Mohamed Hamdan on 2018-Jun-29.
 * mhamdan@nizek.com
 */
@SuppressLint("Registered")
abstract class BaseActivity : AppCompatActivity() {

    private var hasBackButton: Boolean = false
    protected var toolbar: Toolbar? = null

    fun setContentView(layoutResID: Int, hasToolbar: Boolean = false, hasBackButton: Boolean = false) {
        super.setContentView(layoutResID)
        this.hasBackButton = hasBackButton
        if (hasToolbar) {
            setSupportActionBar()
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> if (hasBackButton) {
                onBackPressed()
            }
        }
        return super.onOptionsItemSelected(item)
    }

    private fun setSupportActionBar() {
        toolbar = findViewById(R.id.toolbar)
        if (toolbar != null) {
            setSupportActionBar(toolbar)
            val actionBar = supportActionBar
            if (actionBar != null && hasBackButton) {
                actionBar.setDisplayHomeAsUpEnabled(true)
            }
        }
    }
}
