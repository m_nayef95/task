package com.mohamed.winfooztask.ui.selection

import android.view.View
import android.view.ViewGroup
import com.mohamed.winfooztask.R
import com.mohamed.winfooztask.databinding.RowMajorBinding
import com.mohamed.winfooztask.databinding.RowUniversityBinding
import com.mohamed.winfooztask.models.Major
import com.mohamed.winfooztask.models.University
import com.mohamed.winfooztask.ui.base.BaseAdapter
import com.mohamed.winfooztask.ui.base.BaseViewHolder

class SelectionAdapter<T>(selections: MutableList<T>) : BaseAdapter<T, BaseViewHolder>(selections) {

    companion object {
        private const val SELECT_ALL_TYPE = 2
        private const val MAJOR_TYPE = 3
    }

    override fun onBind(holder: BaseViewHolder, position: Int) {
        if (getItemViewType(position) != SELECT_ALL_TYPE) {
            holder.bind(getItem(position - 1))
        }
        holder.itemView.setOnClickListener {
            val adapterPosition = holder.adapterPosition - 1
            action(getItem(adapterPosition), adapterPosition)
        }
    }

    override fun getViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder {
        return when (viewType) {
            SELECT_ALL_TYPE -> {
                SelectAllViewHolder(inflater.inflate(R.layout.row_select_all, parent, false))
            }
            MAJOR_TYPE -> {
                val binding = RowMajorBinding.inflate(
                        inflater,
                        parent,
                        false
                )
                MajorViewHolder(binding)
            }
            else -> {
                val binding = RowUniversityBinding.inflate(
                        inflater,
                        parent,
                        false
                )
                UniversityViewHolder(binding)
            }
        }
    }

    override fun getItemCount(): Int {
        return if ((adapterItems?.size ?: 0) > 0) super.getItemCount() + 1 else super.getItemCount()
    }

    override fun getItemViewType(position: Int): Int {
        if (position == 0 && (adapterItems?.size ?: 0) > 0) {
            return SELECT_ALL_TYPE
        } else if (getItem(position - 1) is Major) {
            return MAJOR_TYPE
        }
        return super.getItemViewType(position)
    }

    fun updateList(newList: MutableList<T>) {
        setItems(newList)
        notifyDataSetChanged()
    }

    inner class MajorViewHolder(private val binding: RowMajorBinding) : BaseViewHolder(binding.root) {

        override fun bind(item: Any?) {
            binding.major = item as Major
            binding.executePendingBindings()
        }
    }

    inner class UniversityViewHolder(private val binding: RowUniversityBinding) : BaseViewHolder(binding.root) {

        override fun bind(item: Any?) {
            binding.university = item as University
            binding.executePendingBindings()
        }
    }

    inner class SelectAllViewHolder(view: View) : BaseViewHolder(view) {

        override fun bind(item: Any?) {
        }
    }
}