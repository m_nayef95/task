package com.mohamed.winfooztask.ui.main

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import butterknife.ButterKnife
import butterknife.OnClick
import com.mohamed.winfooztask.R
import com.mohamed.winfooztask.ui.base.BaseActivity
import com.mohamed.winfooztask.ui.selection.majors.MajorSelectionActivity
import com.mohamed.winfooztask.ui.selection.universities.UniversitySelectionActivity
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : BaseActivity(), MainContract.View {

    private val actions: MainContract.Actions? by lazy { MainPresenter(this) }

    companion object {
        private const val RC_UNIVERSITY_SELECTION = 1
        private const val RC_MAJOR_SELECTION = 2
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        ButterKnife.bind(this)

        actions?.onCreate()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == RC_UNIVERSITY_SELECTION && resultCode == Activity.RESULT_OK) {
            actions?.onUniversitiesSelected(data?.getIntegerArrayListExtra(UniversitySelectionActivity.UNIVERSITY))
        } else if (requestCode == RC_MAJOR_SELECTION && resultCode == Activity.RESULT_OK) {
            tvActivityMainMajors?.text = getString(R.string.majors_format, data?.getStringExtra(MajorSelectionActivity.MAJORS))
        }
    }

    override fun startMajorSelectionActivity(majors: ArrayList<Int>?) {
        val intent = Intent(this, MajorSelectionActivity::class.java)
        intent.putIntegerArrayListExtra(MajorSelectionActivity.MAJORS, majors)
        startActivityForResult(intent, RC_MAJOR_SELECTION)
    }

    override fun showSelectUniversityToast() {
        Toast.makeText(this, R.string.select_university_first, Toast.LENGTH_LONG).show()
    }

    override fun setUniversities(universities: String) {
        if (universities.isEmpty()) {
            tvActivityMainUniversities?.text = ""
            return
        }
        tvActivityMainUniversities?.text = getString(R.string.universities_format, universities)
    }

    override fun startUniversitySelectionActivity(ids: ArrayList<Int>) {
        val intent = Intent(this, UniversitySelectionActivity::class.java)
        intent.putIntegerArrayListExtra(UniversitySelectionActivity.UNIVERSITIES, ids)
        startActivityForResult(intent, RC_UNIVERSITY_SELECTION)
    }

    @OnClick(R.id.btnActivityMainUniversities)
    fun onUniversitiesClicked() {
        actions?.onUniversitiesClicked()
    }

    @OnClick(R.id.btnActivityMainMajors)
    fun onMajorsClicked() {
        actions?.onMajorClicked()
    }
}
