package com.mohamed.winfooztask.ui.main

import com.mohamed.winfooztask.common.MyApplication
import com.mohamed.winfooztask.models.University

class MainPresenter(val view: MainContract.View?) : MainContract.Actions {

    private var universities: MutableList<University> = mutableListOf()

    override fun onCreate() {
    }

    override fun onUniversitiesSelected(universities: MutableList<Int>?) {
        this.universities.clear()
        this.universities.addAll(MyApplication.instance.universities.filter {
            if (universities?.contains(it.id) == true) {
                it.selected = true
                true
            } else {
                false
            }
        })
        view?.setUniversities(this.universities.joinToString { it.name })
    }

    override fun onMajorClicked() {
        if (universities.isNotEmpty()) {
            view?.startMajorSelectionActivity(ArrayList(universities.map { it.id }))
        } else {
            view?.showSelectUniversityToast()
        }
    }

    override fun onUniversitiesClicked() {
        val ids = universities.filter { it.selected }.map { it.id }
        view?.startUniversitySelectionActivity(ArrayList(ids))
    }
}