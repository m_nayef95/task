package com.mohamed.winfooztask.ui.main

interface MainContract {

    interface Actions {

        fun onCreate()

        fun onUniversitiesSelected(universities: MutableList<Int>?)

        fun onMajorClicked()

        fun onUniversitiesClicked()
    }

    interface View {
        fun startMajorSelectionActivity(majors: ArrayList<Int>?)

        fun showSelectUniversityToast()

        fun setUniversities(universities: String)

        fun startUniversitySelectionActivity(ids: ArrayList<Int>)
    }
}
