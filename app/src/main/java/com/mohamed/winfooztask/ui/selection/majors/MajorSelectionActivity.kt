package com.mohamed.winfooztask.ui.selection.majors

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import butterknife.ButterKnife
import butterknife.OnTextChanged
import com.mohamed.winfooztask.R
import com.mohamed.winfooztask.models.Major
import com.mohamed.winfooztask.ui.base.BaseActivity
import com.mohamed.winfooztask.ui.selection.SelectionAdapter
import kotlinx.android.synthetic.main.activity_major_selection.*

class MajorSelectionActivity : BaseActivity(), MajorSelectionContract.View {

    private val actions: MajorSelectionContract.Actions? by lazy { MajorSelectionPresenter(this) }
    private var adapter: SelectionAdapter<Major>? = null

    companion object {
        const val MAJORS = "majors"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_major_selection, true, true)

        ButterKnife.bind(this)
        actions?.onCreate(intent?.getIntegerArrayListExtra(MAJORS))
    }

    override fun setMajors(majors: MutableList<Major>) {
        adapter = SelectionAdapter(majors)
        rvMajors?.adapter = adapter
        adapter?.setOnItemClickListener { item, _ ->
            if (item == null) {
                actions?.selectAll(adapter?.adapterItems)
                return@setOnItemClickListener
            }
            item.selected = !item.selected
            adapter?.notifyDataSetChanged()
        }
    }

    override fun setNewData(filteredItems: MutableList<Major>) {
        adapter?.updateList(filteredItems)
    }

    override fun goBackWithResult(names: String) {
        val data = Intent()
        data.putExtra(MAJORS, names)
        setResult(Activity.RESULT_OK, data)
        finish()
    }

    override fun notifyAdapter() {
        adapter?.notifyDataSetChanged()
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_selection, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.i_menu_selection -> actions?.onDoneClicked()
        }
        return super.onOptionsItemSelected(item)
    }

    @OnTextChanged(R.id.etActivityMajorSearch)
    fun onSearchTextChanged(text: CharSequence) {
        actions?.onSearchTextChanged(text)
    }
}