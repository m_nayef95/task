package com.mohamed.winfooztask.ui.selection.majors

import com.mohamed.winfooztask.common.MyApplication
import com.mohamed.winfooztask.models.Major
import com.mohamed.winfooztask.models.University

class MajorSelectionPresenter(val view: MajorSelectionContract.View?) : MajorSelectionContract.Actions {

    private val majors = mutableListOf<Major>()

    override fun onCreate(ids: ArrayList<Int>?) {
        val universities: MutableList<University> = MyApplication.instance.universities.filter {
            ids?.contains(it.id) == true
        }.toMutableList()
        majors.addAll(universities.flatMap { it.majors })
        view?.setMajors(majors)
    }

    override fun onDoneClicked() {
        val names = majors.filter { it.selected }.joinToString { it.name }
        view?.goBackWithResult(names)
    }

    override fun onSearchTextChanged(text: CharSequence) {
        view?.setNewData(majors.filter { it.name.contains(text, true) }.toMutableList())
    }

    override fun selectAll(adapterItems: MutableList<Major>?) {
        adapterItems?.forEach { it.selected = true }
        view?.notifyAdapter()
    }
}