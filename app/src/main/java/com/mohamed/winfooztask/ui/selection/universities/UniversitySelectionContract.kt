package com.mohamed.winfooztask.ui.selection.universities

import com.mohamed.winfooztask.models.University

interface UniversitySelectionContract {

    interface Actions {

        fun onSearchTextChanged(text: CharSequence)

        fun onCreate(ids: java.util.ArrayList<Int>?)

        fun onDoneClicked()

        fun selectAll(adapterItems: MutableList<University>?)
    }

    interface View {
        fun setNewData(filteredItems: MutableList<University>)

        fun setUniversities(universities: MutableList<University>)

        fun goBackWithResult(ids: ArrayList<Int>)

        fun notifyAdapter()
    }
}