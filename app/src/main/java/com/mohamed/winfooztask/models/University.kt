package com.mohamed.winfooztask.models

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class University(val id: Int, val name: String, val address: String, val majors: MutableList<Major>, var selected: Boolean) : Parcelable