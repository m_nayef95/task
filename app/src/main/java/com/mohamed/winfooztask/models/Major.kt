package com.mohamed.winfooztask.models

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Major(val id: Int, val name: String, var selected: Boolean) : Parcelable