package com.mohamed.winfooztask.common

import android.app.Application
import com.mohamed.winfooztask.models.University
import com.mohamed.winfooztask.utils.getUniversities

class MyApplication : Application() {

    lateinit var universities: MutableList<University>

    companion object {
        lateinit var instance: MyApplication
    }

    override fun onCreate() {
        super.onCreate()
        instance = this
        universities = getUniversities()
    }


}