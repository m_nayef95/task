package com.mohamed.winfooztask.utils

import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.mohamed.winfooztask.R
import com.mohamed.winfooztask.common.MyApplication
import com.mohamed.winfooztask.models.University
import java.io.BufferedReader

fun getUniversities(): MutableList<University> {
    val gson = Gson()
    val stream = MyApplication.instance.resources.openRawResource(R.raw.universities)
    var allText = ""
    stream.use {
        allText = it.bufferedReader().use(BufferedReader::readText)
    }
    return gson.fromJson(allText, object : TypeToken<MutableList<University>>() {}.type)
}